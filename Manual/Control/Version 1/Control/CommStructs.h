//These are the structures responsible for communication

//data to be sent to the robot for control purposes
struct ControlData
{
  int leftMotorSpeed;
  int rightMotorSpeed;
  int actuatorSpeed;
  int stateControl;
};

//data storing current state of the robot
struct StateData
{
  int errorState;
  int controlState;
};

EasyTransfer communicationBoardOut; //for sending commands to the robot
EasyTransfer communicationBoardIn;  //for receiving data back from the robot

ControlData control;  //control data to send to robot
StateData state;

void initializeCommunicaton()
{
   Serial1.begin(34800);
   
   communicationBoardOut.begin(details(control), &Serial1);
   communicationBoardIn.begin(details(state), &Serial1);
}

void updateCommunication()
{
  communicationBoardOut.sendData();
  communicationBoardIn.receiveData();
}
