//Version 1
//Created by Duncan Campbell and Grant Carroll

#include "EasyTransfer.h"
#include "digitalWriteFast.h"

#include "CommStructs.h"
#include "PinMap.h"

void setup()
{
  //initalize the communication
  initializeCommunicaton();
}

void loop()
{
  updateControls();
  updateCommunication();
}
