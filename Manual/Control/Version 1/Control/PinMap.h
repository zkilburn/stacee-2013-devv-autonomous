//These are the settings for all of the pins

//analog joystick pins
#define JOYSTICK_1_X A10
#define JOYSTICK_1_Y A11
#define JOYSTICK_2_X A12
#define JOYSTICK_2_Y A13

//set the digital io pins
#define ACTUATOR_UP 24 
#define ACTUATOR_DOWN 23
#define ESTOP 22

//Motor Control Variables
int actuatorSpeed = 127;
int leftMotorSpeed = 127;
int rightMotorSpeed = 127;

void initializePins()
{
  pinModeFast(ACTUATOR_UP, INPUT);
  pinModeFast(ACTUATOR_DOWN, INPUT);
  pinModeFast(ESTOP, INPUT);
}

//update all the analog inputs
static void analogUpdate()
{
  leftMotorSpeed = map(analogRead(JOYSTICK_1_Y), 0, 1023, 1, 255);
  rightMotorSpeed = map(analogRead(JOYSTICK_2_Y), 0, 1023, 1, 255);  
}

static void digitalUpdate()
{
  //actuator control code
  if(digitalReadFast(ACTUATOR_UP) == HIGH)
  {
    actuatorSpeed = 255;
  }
  else if(digitalReadFast(ACTUATOR_DOWN) == HIGH)
  {
    actuatorSpeed = 1;
  }
  else
  {
    actuatorSpeed = 127;
  }
  
  //stop the f***ing robot fast
  if(digitalReadFast(ESTOP) == HIGH)
  {
    leftMotorSpeed = 0;
    rightMotorSpeed = 0;
    actuatorSpeed = 0;
  }
}
  

void updateControls()
{
  analogUpdate();
  digitalUpdate();
}
