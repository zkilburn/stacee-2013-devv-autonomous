//These are the structures responsible for communication

//data to be received from the robot for control purposes
struct ControlData
{
  int leftMotorSpeed;
  int rightMotorSpeed;
  int actuatorSpeed;
  int stateControl;
};

//data storing the current state of the robot
struct StateData
{
  int errorState;
  int controlState;
};

struct PowerData
{
  boolean refresh;
  int amphours;
};

EasyTransfer controlBoardOut;
EasyTransfer controlBoardIn;

ControlData control;
StateData state;

void initializeCommunicaton()
{
  Serial1.begin(34800);
  
  controlBoardOut.begin(details(state), &Serial1);
  controlBoardIn.begin(details(control), &Serial1);
}

void updateCommunication()
{
  controlBoardOut.sendData();
  controlBoardIn.receiveData();  
}
