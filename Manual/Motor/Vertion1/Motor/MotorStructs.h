// previous millis for motors
long previousMillis = 0;
//previous millis for actuator
long previousMillis2 = 0;
//iniciall motor speed
int motorRight = 127;
int motorLeft = 127;
int actuator = 127;
//time interval for motors
int intervalMotor = 20;
//time interval for actuator
int intervalActuator = 10;
// com check true if ok, false if coms bad
boolean comUP = true;



//motor control data recieved
struct MotorData
{
  int leftMotorSpeed;
  int rightMotorSpeed;
  int actuatorSpeed;
};


MotorData control;

//inicialise communication with comm board
EasyTransfer motorBoardIn;

void initializeCommunicaton()
{
  Serial.begin(34800);
  motorBoardIn.begin(details(control), &Serial);
  
}




//inline 
void comSafety(boolean check)
{
  static unsigned long current = 0;
  static unsigned long last_com = 0;
  
  current = millis();
  
  if(check)
  {
    last_com = current;
    comUP = true;
  }
  
  if(current - last_com > 1000)
  {
    motorRight = 0;
    motorLeft = 0;
    actuator = 0;
    comUP = false;
  }
}

void updateCommunication()
{
  if (motorBoardIn.receiveData())
  {
    comSafety(true);
  }
  else
  {
    comSafety(false);
  }
}
