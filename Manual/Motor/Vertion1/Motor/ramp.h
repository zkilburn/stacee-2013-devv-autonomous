

void ramp()
{
  long Millis = millis();
  if ( (control.leftMotorSpeed == 0) || (control.rightMotorSpeed == 0) || (control.actuatorSpeed == 0) || (!comUP))
  {
    motorRight = 0;
    motorLeft = 0;
    actuator = 0;
  }
  else
  {
    if ((Millis - previousMillis) > intervalMotor)
    {
      previousMillis = Millis;

      if(control.leftMotorSpeed != motorLeft)
      {

        if (control.leftMotorSpeed > motorLeft)
        {
          motorLeft++;
        }

        if (control.leftMotorSpeed < motorLeft)
        {
          motorLeft--;
        }

      }


      if(control.rightMotorSpeed != motorRight)
      {

        if (control.rightMotorSpeed > motorRight)
        {
          motorRight++;
        }

        if (control.rightMotorSpeed < motorRight)
        {
          motorRight--;
        }

      }

    }
    Millis = millis();
    if ((Millis - previousMillis2) > intervalActuator)
    {
      if(control.actuatorSpeed != actuator)
      {

        if (control.actuatorSpeed > actuator)
        {
          actuator++;
        }

        if (control.actuatorSpeed < actuator)
        {
          actuator--;
        }

      }
    }
  }
}


