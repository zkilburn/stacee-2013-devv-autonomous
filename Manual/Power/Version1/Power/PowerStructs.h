

EasyTransfer powerBoardOut;
EasyTransfer powerBoardIn;

struct CommandData
{
  boolean refresh;
};

struct PowerData
{
  long amphours;
  long watthours;
};

CommandData request;
PowerData data;

void initializeCommunicaton()
{
  Serial.begin(34800);
  powerBoardOut.begin(details(data), &Serial);
  powerBoardIn.begin(details(request), &Serial);
}



void calculatePower()
{
  unsigned long totalTime = timeintervals * 20;
  int hours = totalTime / 100 / 60 / 60;
  data.amphours = totalAmps * hours;
  int averageAmps = totalAmps / timeintervals;
  int averageVolts = totalVolts / timeintervals;
  long averageWatt = totalVolts * timeintervals;
  data.watthours = averageWatt * hours;
  
}

void updateCommunication()
{
  powerBoardIn.receiveData();
  if(request.refresh)
  {
    calculatePower();
    powerBoardOut.sendData();
  }
  
}

