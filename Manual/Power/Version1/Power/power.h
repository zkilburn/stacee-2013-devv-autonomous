unsigned long totalAmps = 0;
unsigned long timeintervals = 0;
unsigned long totalVolts = 0;
int currentAmps;
int lastAmps = 0;
int currentVolts;
int lastVolts = 0;
long previousMillis = 0;

#define volts A1 //actual pin values need filled in
#define amps A0

void power()
{
  long currentMillis = millis();
  if ((currentMillis - previousMillis) > 20)
  {
    currentAmps = map(analogRead(amps),0,1024,0,12); //the actual values need added when we know what they are
    currentVolts = map(analogRead(volts),0,1024,0,12); //the actual values need added when we know what they are
    totalAmps = totalAmps + ((currentAmps + lastAmps) / 2);
    totalVolts = totalVolts + ((currentVolts + lastVolts) / 2);
    lastAmps = currentAmps;
    lastVolts = currentVolts;
    previousMillis = currentMillis;
    timeintervals++;
  }
  
}


